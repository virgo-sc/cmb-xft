<?php

use CMB\XFT\BinParse\BinParse;
use CMB\XFT\BinParse\BInParseList;
use CMB\XFT\Request\XFTBehalfBatchStatementRequest;
use CMB\XFT\XFTConstruct;


require_once 'vendor/autoload.php';

$config = [
    //测试地址
    'gateway' => 'https://api.cmbchina.com/xft/apm',
    'appid' => '84605***************6a5c',          //Open API 平台应用编号
    'app_secret' => '4aa1941b*************1d9542c',     //Open API 平台应用秘钥
    'sm2_private_key' => '6b7bd********************35aea',//Open API 平台上传的SM2 公钥对应的私钥
    'xft_appId' => 'XFY*****',      //薪福通应用 ID
    'xft_app_secret' => '**************', //薪福通应用秘钥
    'company_id' => 'XFY****',     //薪福通企业号
    'usr_uid' => 'AUTO0001',        //薪福通平台用户号 AUTO0001
    'usr_nbr' => 'A0001',        //薪福通企业用户号 A0001
    'logs_dir' => './xftLog',
    'timeout' => 200,

    'JAVA_HOME' => 'C:\Users\Administrator\.jdks\corretto-1.8.0_352\bin\java.exe',
    'JAR_PATH' => 'D:\work\cmbxft-java\out\artifacts\cmbxft_java_jar\cmbxft-java.jar'
];


$xft = new XFTConstruct($config);
$xft->setTimeout(10);
$xft->setProxy('');
$xft->setCharset('uft-8');
$xft->setFormat('json');
$xft->setHttpDebug(false);
$xft->setSignType('SM2');
$xft->setGateway($config['gateway']);
$xft->setLogsDir($config['logs_dir']);

//获取代发协议
if (0) {
    $response = $xft->agencyAgreementQuery();
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getAgencyList());
    var_dump($response->accountCheck('12345678945623'));
    exit;
}
//卡BIN解析
if (0) {
    $list = new BInParseList();
    $one = new BinParse();
    $one->setName('刘吴');
    $one->setAccount('62260041021102212012');
    $one->setNo('1');
    $one->setAmount('100');

    $list->append($one);


    $response = $xft->binParse($list);
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getBinList());
    exit;
}
//可用支付用户查询
if (0) {
    $response = $xft->payUser();

    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getPayUserList());
    exit;
}
//代发支付
if (0) {
    $batchNo = '202208031516121333'; //批次号
    $phone = '18265636269';

    //支付方
    $payer = new \CMB\XFT\Behalf\Payer();
    $payer->setAccount('2222232'); //账号
    $payer->setName('text');//户名
    $payer->setAmount(1);
    $payer->setCount(2);
    $payer->setMark('代发说明');

    $payer->setBatchNo($batchNo);
    $payer->setPhone($phone);
    $payer->setPayValidity(2);

    //收款方
    $payee = new \CMB\XFT\Behalf\Payee();
    $payee->setBatchNo($batchNo);

    $payeeList = new \CMB\XFT\Behalf\PayeeList();
    $payeeList->append($payee);

    $response = $xft->behalfPayment($payer, $payeeList);

    var_dump($response->isSuccess());
    var_dump($response->getBody());
    var_dump($response->getPayInfo());
    var_dump($response->getPayUrl());
    exit;
}
//代发概要查询 批次情况单条查询
if (0) {
    $batchNo = '2323232';//客户批次号(不可重复)
    $batchTransId = '3232';//招行批次号(不可重复) //两者都不传为列表查询
    $beginDate = '2022-10-20';
    $endAtDate = '2022-10-30';

    $response = $xft->behalfBatchQuery($beginDate, $endAtDate, $batchNo, $batchTransId, '1', '10000');

    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getAmount());
    var_dump($response->getCount());
    var_dump($response->getStatus());
    var_dump($response->getBatchInfo());
    exit;
}
//代发概要查询 批次情况列表查询
if (0) {
    $beginDate = '2022-10-20';
    $endAtDate = '2022-10-30';
    $response = $xft->behalfBatchListQuery($beginDate, $endAtDate, '1', '10000');

    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getBatchInfo());
    exit;
}
//代发资金查询
if (0) {
    $agencyAgreement = 'ZO7727';
    $response = $xft->behalfMoneyQuery($agencyAgreement);
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getAccountList());
    exit;
}

//代发明细
if (0) {
    $batchNo = 'aaaa';//客户批次号(不可重复)
    $batchTransId = 'bbb';//招行批次号(不可重复)
    $page = '1';
    $pageSize = '10000';

    $response = $xft->behalfBatchInfo($batchNo, $batchTransId, $page, $pageSize);
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getInfoList());

    exit;
}
//对账单查询
if (0) {
    $beginDate = '2022-10-20';
    $endAtDate = '2022-10-30';
    $agencyAgreement = 'AG4993';

    $batchNo = 'aaaa';//客户批次号(不可重复)
    $batchTransId = 'bbb';//招行批次号(不可重复)
    $page = '1';
    $pageSize = '10000';

    $response = $xft->batchStatement($beginDate, $endAtDate, $batchNo, $batchTransId, $agencyAgreement, $page, $pageSize);

    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getList());
    exit;
}
//生成对账单PDF 汇总
if (0) {
    $batchNo = 'aaaa';//客户批次号(不可重复)
    $batchTransId = 'bbb';//招行批次号(不可重复)
    $orderId = 'orderId-22323';  //代发明细序号
    $agreement = '';

    $response = $xft->behalfBatchPDFApply($agreement,$batchNo, $batchTransId, $orderId);

    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getRedisKey());
    exit;
}
//生成对账单PDF 逐笔明细
if (0) {
    $batchNo = 'aaaa';//客户批次号(不可重复)
    $batchTransId = 'bbb';//招行批次号(不可重复)
    $orderId = 'orderId-22323';  //代发明细序号
    $agreement = '';

    $response = $xft->behalfInfoPDFApply($agreement, $batchNo, $batchTransId, $orderId);
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getRedisKey());
    exit;
}
//查询PDF的生成的状态/进度服务
if (0) {
    $redisKey = '323232';

    $response = $xft->behalfPDFSchedule($redisKey);
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getRedisKey());
    var_dump($response->getStatus());
    var_dump($response->getProgress());
    exit;
}
//对账单PDF下载服务
if (0) {
    $redisKey = '322dsaf';
    $response = $xft->behalfPDFDown($redisKey, 0);
    var_dump($response->isSuccess());
    var_dump($response->getError());
    var_dump($response->getIsFinish());
    var_dump($response->getNextBlockNo());
    var_dump($response->getFileData());
    var_dump($response->getFileType());
    exit;

}
