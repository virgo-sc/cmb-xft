<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfBatchInfoResponse extends XFTBaseResponse
{
    /**
     * batchInfo
     * @var array $batchInfo
     */
    private $infoList = [];

    public function resolve(): response
    {
        parent::resolve();

        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->pageInfo();

        $this->infoList = $body['EAIDTLSTZ'] ?? [];


        return $this;
    }

    /**
     * @return array
     */
    public function getInfoList(): array
    {
        return $this->infoList;
    }
}