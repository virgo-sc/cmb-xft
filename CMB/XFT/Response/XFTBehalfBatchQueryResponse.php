<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfBatchQueryResponse extends XFTBaseResponse
{
    /**
     * batchInfo
     * @var array $batchInfo
     */
    private $batchInfo = [];

    private $amount = '';

    private $count = '';

    private $status = '';

    private $payError = '';

    public function resolve(): response
    {
        parent::resolve();

        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->pageInfo();

        $this->batchInfo = $body['EAIMALSTZ'][0] ?? [];

        $this->amount = $this->batchInfo['TRXAMT'] ?? '';
        $this->count = $this->batchInfo['TRXNUM'] ?? '';
        $this->status = $this->batchInfo['STSCOD'] ?? '';
        $this->payError = $this->batchInfo['PAYINF'] ?? '';

        return $this;
    }

    /**
     * @return array
     */
    public function getBatchInfo(): array
    {
        return $this->batchInfo;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCount(): string
    {
        return $this->count;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getPayError(): string
    {
        return $this->payError;
    }
}