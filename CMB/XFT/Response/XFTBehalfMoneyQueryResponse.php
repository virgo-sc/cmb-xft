<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfMoneyQueryResponse extends XFTBaseResponse
{
    /**
     * @var array
     */
    private $accountList = [];


    public function resolve(): response
    {
        parent::resolve();
        if ($this->getError()) {
            return $this;
        }
        $body = $this->getbody();

        $list = $body['EASPCQRYZ'] ?? [];

        $this->accountList = $list;

        return $this;
    }

    /**
     * @return array
     */
    public function getAccountList(): array
    {
        return $this->accountList;
    }
}