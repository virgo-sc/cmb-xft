<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfInfoPDFApplyResponse extends XFTBaseResponse
{
    /**
     * batchInfo
     * @var array $batchInfo
     */
    private $redisKey = '';

    public function resolve(): response
    {
        parent::resolve();

        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->pageInfo();

        $this->redisKey = $body['EAISMYPDFZ'][0]['RESKEY'] ?? [];


        return $this;
    }

    /**
     * @return array
     */
    public function getRedisKey()
    {
        return $this->redisKey;
    }
}