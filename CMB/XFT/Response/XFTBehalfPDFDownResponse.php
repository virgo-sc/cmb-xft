<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfPDFDownResponse extends XFTBaseResponse
{
    private $status = '';

    private $nextBlockNo = '';

    private $fileType = '';

    private $fileData = '';

    private $isFinish = false;

    public function resolve(): response
    {
        parent::resolve();

        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->status = $body['EAIPDFDLY'][0]['FINFLG'] ?? '';
        $this->nextBlockNo = $body['EAIPDFDLY'][0]['FILINX'] ?? '';

        $this->fileData = $body['EAIPDFDLZ'][0]['FILDTA'] ?? '';
        $this->fileType = $body['EAIPDFDLZ'][0]['FILTYP'] ?? '';

        $this->isFinish = $this->status == 'Y';

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getNextBlockNo(): string
    {
        return $this->nextBlockNo;
    }

    /**
     * @return string
     */
    public function getFileType(): string
    {
        return $this->fileType;
    }

    /**
     * @return string
     */
    public function getFileData(): string
    {
        return $this->fileData;
    }

    /**
     * @return bool
     */
    public function getIsFinish(): bool
    {
        return $this->isFinish;
    }
}