<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfPaymentResponse extends XFTBaseResponse
{
    /**
     * @var array
     */
    private $payInfo = [];

    /**
     * @var string
     */
    private $payUrl = '';

    /**
     * @var string
     */
    private $payNo = '';

    /**
     * @var string
     */
    private $payStatus = '';


    public function resolve(): response
    {
        parent::resolve();
        if ($this->getError()) {
            return $this;
        }
        $body = $this->getbody();

        $this->payInfo = $body['EAIAGPAYZ'][0] ?? [];

        $this->payNo = $this->payInfo['TRSREF'] ?? '';
        $this->payStatus = $this->payInfo['STSCOD'] ?? '';

        $code = $this->payInfo['ERRCOD'] ?? '';

        if ($code != '0000000') {
            $this->setError($this->payInfo['ERRMSG'] ?? '');
        }

        $this->payUrl = $body['EAIAGPURZ'][0]['PAYURL'] ?? '';

        return $this;
    }

    /**
     * @return array
     */
    public function getPayInfo(): array
    {
        return $this->payInfo;
    }

    /**
     * @return string
     */
    public function getPayUrl(): string
    {
        return $this->payUrl;
    }

    /**
     * @return string
     */
    public function getPayStatus(): string
    {
        return $this->payStatus;
    }

    /**
     * @return string
     */
    public function getPayNo(): string
    {
        return $this->payNo;
    }
}