<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBinParseResponse extends XFTBaseResponse
{
    /**
     * @var array
     */
    private $binList = [];


    public function resolve(): response
    {
        parent::resolve();
        if ($this->getError()) {
            return $this;
        }
        $body = $this->getbody();


        $list = $body['EAIDTCHKZ'] ?? [];

        $this->binList = $list;

        return $this;
    }

    /**
     * @return array
     */
    public function getBinList(): array
    {
        return $this->binList;
    }
}