<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBaseResponse extends Response
{

    private $page = '';

    private $pageSize = '';

    private $totalPage = '';

    private $errorMsg;

    private $errorCode;

    public function resolve(): Response
    {
        if (json_decode($this->getBody())) {
            $body = json_decode($this->getBody(), true);
            $this->setBody($body);
            if ($body['SYCOMRETZ'] ?? '' and count($body['SYCOMRETZ']) == 1) {
                $this->errorMsg = $body['SYCOMRETZ'][0]['ERRMSG'] ?? '';
                $this->errorCode = $body['SYCOMRETZ'][0]['ERRCOD'] ?? '';
                if ($this->errorCode == '0000000') {
                    return $this;
                }
                $dtl = $body['SYCOMRETZ'][0]['ERRDTL'] ?? '';
                $this->setError($this->errorMsg . '-' . $this->errorCode . "-" . $dtl);
            }
        } else {
            $this->setError($this->getBody());
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function pageInfo()
    {
        $pageInfo = $this->get('SYPAGINFY')[0] ?? [];

        $this->page = $pageInfo['PAGNBR'] ?? '';
        $this->pageSize = $pageInfo['PGENUM'] ?? '';
        $this->totalPage = $pageInfo['TOTSIZ'] ?? '';
    }
}