<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfBatchQueryListResponse extends XFTBaseResponse
{
    /**
     * batchInfo
     * @var array $batchInfo
     */
    private $batchInfo = [];

    public function resolve(): response
    {
        parent::resolve();

        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->pageInfo();

        $this->batchInfo = $body['EAIMALSTZ'] ?? [];


        return $this;
    }

    /**
     * @return array
     */
    public function getBatchInfo(): array
    {
        return $this->batchInfo;
    }
}