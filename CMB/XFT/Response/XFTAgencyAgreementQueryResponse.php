<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTAgencyAgreementQueryResponse extends XFTBaseResponse
{
    /**
     * @var array
     */
    private $agencyList = [];


    public function resolve(): response
    {
        parent::resolve();
        if ($this->getError()) {
            return $this;
        }
        $body = $this->getbody();

        $list = $body['EAIAGCNVZ'] ?? [];
        $this->agencyList = $list;

        return $this;
    }

    /**
     * @return array
     */
    public function getAgencyList(): array
    {
        return $this->agencyList;
    }

    /**
     * 账户存在检查
     * @param $account
     * @return bool
     */
    public function accountCheck($account): bool
    {
        $list = array_column($this->agencyList, 'EACNBR');

        return key_exists($account, $list);
    }
}