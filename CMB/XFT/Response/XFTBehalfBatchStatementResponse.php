<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfBatchStatementResponse extends XFTBaseResponse
{
    /**
     * batchInfo
     * @var array $batchInfo
     */
    private $list = [];

    public function resolve(): response
    {
        parent::resolve();

        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->pageInfo();

        $this->list = $body['EAIRPLSTZ'] ?? [];


        return $this;
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }


}