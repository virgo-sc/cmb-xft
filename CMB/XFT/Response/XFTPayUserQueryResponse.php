<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTPayUserQueryResponse extends XFTBaseResponse
{
    /**
     * @var array
     */
    private $payUserList = [];


    public function resolve(): response
    {
        parent::resolve();
        if ($this->getError()) {
            return $this;
        }
        $body = $this->getbody();

        $list = $body['EAIPAYUSZ'] ?? [];

        $this->payUserList = $list;

        return $this;
    }

    /**
     * @return array
     */
    public function getPayUserList(): array
    {
        return $this->payUserList;
    }


}