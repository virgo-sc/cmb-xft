<?php

namespace CMB\XFT\Response;

use CMB\Http\Response;

class XFTBehalfPDFScheduleResponse extends XFTBaseResponse
{
    /**
     * batchInfo
     * @var array $batchInfo
     */
    private $redisKey = '';

    private $status = '';

    private $progress = '';

    public function resolve(): response
    {
        parent::resolve();


        if ($this->getError()) {
            return $this;
        }

        $body = $this->getbody();

        $this->pageInfo();

        $this->redisKey = $body['EAIQRPSTZ'][0]['RESKEY'] ?? [];
        $this->status = $body['EAIQRPSTZ'][0]['HASFIN'] ?? [];
        $this->progress = $body['EAIQRPSTZ'][0]['PROGRE'] ?? [];

        return $this;
    }

    /**
     * @return array
     */
    public function getRedisKey()
    {
        return $this->redisKey;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getProgress(): string
    {
        return $this->progress;
    }
}