<?php

namespace CMB\XFT;

/**
 * 招行-薪福通
 */

use Alipay\AlipayFund\Info\Parties;
use CMB\XFT\Behalf\PayeeList;
use CMB\XFT\Behalf\Payer;
use CMB\XFT\BinParse\BInParseList;
use CMB\XFT\Request\XFTAgencyAgreementQueryRequest;
use CMB\XFT\Request\XFTBehalfBatchInfoRequest;
use CMB\XFT\Request\XFTBehalfBatchPDFApplyRequest;
use CMB\XFT\Request\XFTBehalfBatchQueryListRequest;
use CMB\XFT\Request\XFTBehalfBatchQueryRequest;
use CMB\XFT\Request\XFTBehalfBatchStatementRequest;
use CMB\XFT\Request\XFTBehalfInfoPDFApplyRequest;
use CMB\XFT\Request\XFTBehalfMoneyQueryRequest;
use CMB\XFT\Request\XFTBehalfPDFDownRequest;
use CMB\XFT\Request\XFTBehalfPDFScheduleRequest;
use CMB\XFT\Request\XFTBinParseRequest;
use CMB\XFT\Request\XFTBehalfPaymentRequest;
use CMB\XFT\Request\XFTPayUserQueryRequest;
use CMB\XFT\Response\XFTAgencyAgreementQueryResponse;
use CMB\XFT\Response\XFTBehalfBatchPDFApplyResponse;
use CMB\XFT\Response\XFTBehalfBatchQueryListResponse;
use CMB\XFT\Response\XFTBehalfBatchQueryResponse;
use CMB\XFT\Response\XFTBehalfBatchInfoResponse;
use CMB\XFT\Response\XFTBehalfBatchStatementResponse;
use CMB\XFT\Response\XFTBehalfInfoPDFApplyResponse;
use CMB\XFT\Response\XFTBehalfMoneyQueryResponse;
use CMB\XFT\Response\XFTBehalfPaymentResponse;
use CMB\XFT\Response\XFTBehalfPDFDownResponse;
use CMB\XFT\Response\XFTBehalfPDFScheduleResponse;
use CMB\XFT\Response\XFTBinParseResponse;
use CMB\XFT\Response\XFTPayUserQueryResponse;

class XFTConstruct extends Builder
{
    public function __construct(array $config)
    {
        $this->gateway = $config['gateway'] ?? '';

        $this->appId = $config['appid'] ?? '';

        $this->app_secret = $config['app_secret'] ?? '';

        $this->sm2PrivateKey = $config['sm2_private_key'] ?? '';

        $this->xftAppId = $config['xft_appId'] ?? '';

        $this->xftAppSecret = $config['xft_app_secret'] ?? '';

        $this->companyId = $config['company_id'] ?? '';

        $this->usrUid = $config['usr_uid'] ?? '';

        $this->usrNbr = $config['usr_nbr'] ?? '';

        $this->logsDir = $config['logs_dir'] ?? '';

        $this->timeout = $config['timeout'] ?? '';

        $this->javaHome = $config['JAVA_HOME'] ?? '';

        $this->jarPath = $config['JAR_PATH'] ?? '';
    }


    public function agencyAgreementQuery(): XFTAgencyAgreementQueryResponse
    {
        $agencyAgreementQueryRequest = new XFTAgencyAgreementQueryRequest();

        /*** @var $response  XFTAgencyAgreementQueryResponse */
        $response = $this->execute($agencyAgreementQueryRequest);

        return $response;
    }

    //卡bin解析
    public function binParse(BInParseList $bInParseList): XFTBinParseResponse
    {
        $binParse = new XFTBinParseRequest();
        $binParse->setList($bInParseList);

        /*** @var $response XFTBinParseResponse */
        $response = $this->execute($binParse);

        return $response;
    }

    //查询可用支付用户API服务
    public function payUser(): XFTPayUserQueryResponse
    {
        $request = new XFTPayUserQueryRequest();

        /*** @var $response XFTPayUserQueryResponse */
        $response = $this->execute($request);

        return $response;

    }

    //代发支付
    public function behalfPayment(Payer $payer, PayeeList $payeeList): XFTBehalfPaymentResponse
    {
        $request = new XFTBehalfPaymentRequest();

        $request->setPayer($payer);
        $request->setPayeeList($payeeList);

        /*** @var $response XFTBehalfPaymentResponse */
        $response = $this->execute($request);

        return $response;
    }

    //代发批次支付情况查询单条
    public function behalfBatchQuery($beginDate, $endAtDate, $batchNo, $batchTransId, $page, $pageSize): XFTBehalfBatchQueryResponse
    {
        $request = new XFTBehalfBatchQueryRequest();
        $request->setBeginDate($beginDate);
        $request->setEndAtDate($endAtDate);
        $request->setBatchNo($batchNo);
        $request->setBatchTransId($batchTransId);
        $request->setPage($page);
        $request->setPageSize($pageSize);

        /*** @var $response XFTBehalfBatchQueryResponse */
        $response = $this->execute($request);

        return $response;
    }

    //代发批次支付情况查询列表
    public function behalfBatchListQuery($beginDate, $endAtDate, $page, $pageSize): XFTBehalfBatchQueryListResponse
    {
        $request = new XFTBehalfBatchQueryListRequest();
        $request->setBeginDate($beginDate);
        $request->setEndAtDate($endAtDate);
        $request->setPage($page);
        $request->setPageSize($pageSize);

        /*** @var $response XFTBehalfBatchQueryListResponse */
        $response = $this->execute($request);

        return $response;
    }

    //资金查询
    public function behalfMoneyQuery($agencyAgreement = ''): XFTBehalfMoneyQueryResponse
    {
        $request = new XFTBehalfMoneyQueryRequest();
        $request->setAgencyAgreement($agencyAgreement);

        /*** @var $response XFTBehalfMoneyQueryResponse */
        $response = $this->execute($request);

        return $response;
    }

    //代发批次支付情况查询列表
    public function behalfBatchInfo($batchNo, $batchTransId, $page, $pageSize): XFTBehalfBatchInfoResponse
    {
        $request = new XFTBehalfBatchInfoRequest();
        $request->setBatchNo($batchNo);
        $request->setBatchTransId($batchTransId);
        $request->setPage($page);
        $request->setPageSize($pageSize);

        /*** @var $response XFTBehalfBatchInfoResponse */
        $response = $this->execute($request);

        return $response;
    }

    //对账单列表
    public function batchStatement($beginDate, $endAtDate, $batchNo, $batchTransId, $agreement, $page, $pageSize): XFTBehalfBatchStatementResponse
    {
        $request = new XFTBehalfBatchStatementRequest();
        $request->setBatchNo($batchNo);
        $request->setBatchTransId($batchTransId);
        $request->setAgreement($agreement);
        $request->setBeginDate($beginDate);
        $request->setEndAtDate($endAtDate);
        $request->setPage($page);
        $request->setPageSize($pageSize);

        /*** @var $response XFTBehalfBatchStatementResponse */
        $response = $this->execute($request);

        return $response;
    }

    //申请PDF 汇总
    public function behalfBatchPDFApply($agreement, $batchNo, $batchTransId, $orderId): XFTBehalfBatchPDFApplyResponse
    {
        $request = new XFTBehalfBatchPDFApplyRequest();
        $request->setAgreement($agreement);
        $request->setBatchNo($batchNo);
        $request->setBatchTransId($batchTransId);
        $request->setOrderId($orderId);

        /*** @var $response XFTBehalfBatchPDFApplyResponse */
        $response = $this->execute($request);

        return $response;
    }

    //申请PDF 逐笔明细
    public function behalfInfoPDFApply($agreement, $batchNo, $batchTransId, $orderId): XFTBehalfInfoPDFApplyResponse
    {
        $request = new XFTBehalfInfoPDFApplyRequest();
        $request->setBatchNo($batchNo);
        $request->setBatchTransId($batchTransId);
        $request->setOrderId($orderId);
        $request->setAgreement($agreement);

        /*** @var $response XFTBehalfInfoPDFApplyResponse */
        $response = $this->execute($request);

        return $response;
    }

    //查询PDF的生成的状态/进度服务
    public function behalfPDFSchedule($redisKey): XFTBehalfPDFScheduleResponse
    {
        $request = new XFTBehalfPDFScheduleRequest();
        $request->setRedisKey($redisKey);

        /*** @var $response XFTBehalfPDFScheduleResponse */
        $response = $this->execute($request);

        return $response;
    }

    //对账单PDF下载服务
    public function behalfPDFDown($redisKey, $blockNo = 0): XFTBehalfPDFDownResponse
    {
        $request = new XFTBehalfPDFDownRequest();
        $request->setRedisKey($redisKey);
        $request->setBlockNo($blockNo);

        /*** @var $response XFTBehalfPDFDownResponse */
        $response = $this->execute($request);

        return $response;

    }
}