<?php

namespace CMB\XFT\Behalf;

class PayeeList
{
    protected $list;

    public function append(Payee $payee)
    {
        $this->list[] = $payee->generate();
    }

    public function generate()
    {
        return $this->list;
    }


}