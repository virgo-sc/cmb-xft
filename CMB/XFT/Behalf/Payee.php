<?php

namespace CMB\XFT\Behalf;

class Payee
{

    /**
     * 客户系统代发批次号
     * @var string
     */
    private $batchNo = '';

    /**
     * 代发明细序号
     * @var string
     */
    private $paymentId = '';

    /**
     * 账号
     * @var string
     */
    private $account = '';

    /**
     * 账号名称
     * @var string
     */
    private $name = '';

    /**
     * 账户类型
     * @var string
     */
    private $type = '0';

    /**
     * 开户行编码
     * @var string
     */
    private $bankCode = '';

    /**
     * 开户行名称
     * @var string
     */
    private $bank = '';

    /**
     * 交易金额
     * @var string
     */
    private $amount = '';

    /**
     * 摘要
     * @var string
     */
    private $txt = '';

    /**
     * 他行户口行号
     * @var string
     */
    private $bankNum = '';

    /**
     * 开户地
     * @var string
     */
    private $bankAddress = '';

    /**
     * 它行户口所在省
     * @var string
     */
    private $bankProvince = '';

    /**
     * 它行户口所在市
     * @var string
     */
    private $bankCity = '';

    /**
     * 代发渠道
     * @var string
     */
    private $channel = '';



    public function generate(): array
    {
        return [
            'BUSREF' => $this->batchNo,
            'TRXREF' => $this->paymentId,
            'EACNBR' => $this->account,
            'EACNAM' => $this->name,
            'EACTYP' => $this->type,
            'BNKTYP' => $this->bankCode,
            'EACBNK' => $this->bank,
            'TRXAMT' => $this->amount,
            'TRXTXT' => $this->txt,
            'RCVBNK' => $this->bankNum,
            'EACCTY' => $this->bankAddress,
            'EACPRV' => $this->bankProvince,
            'EACCIT' => $this->bankCity,
            'AGTCHL' => $this->channel,
        ];
    }

    /**
     * @return string
     */
    public function getBatchNo(): string
    {
        return $this->batchNo;
    }

    /**
     * @param string $batchNo
     */
    public function setBatchNo(string $batchNo): void
    {
        $this->batchNo = $batchNo;
    }

    /**
     * @return string
     */
    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    /**
     * @param string $paymentId
     */
    public function setPaymentId(string $paymentId): void
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount(string $account): void
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getBankCode(): string
    {
        return $this->bankCode;
    }

    /**
     * @param string $bankCode
     */
    public function setBankCode(string $bankCode): void
    {
        $this->bankCode = $bankCode;
    }

    /**
     * @return string
     */
    public function getBank(): string
    {
        return $this->bank;
    }

    /**
     * @param string $bank
     */
    public function setBank(string $bank): void
    {
        $this->bank = $bank;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getTxt(): string
    {
        return $this->txt;
    }

    /**
     * @param string $txt
     */
    public function setTxt(string $txt): void
    {
        $this->txt = $txt;
    }

    /**
     * @return string
     */
    public function getBankNum(): string
    {
        return $this->bankNum;
    }

    /**
     * @param string $bankNum
     */
    public function setBankNum(string $bankNum): void
    {
        $this->bankNum = $bankNum;
    }

    /**
     * @return string
     */
    public function getBankAddress(): string
    {
        return $this->bankAddress;
    }

    /**
     * @param string $bankAddress
     */
    public function setBankAddress(string $bankAddress): void
    {
        $this->bankAddress = $bankAddress;
    }

    /**
     * @return string
     */
    public function getBankProvince(): string
    {
        return $this->bankProvince;
    }

    /**
     * @param string $bankProvince
     */
    public function setBankProvince(string $bankProvince): void
    {
        $this->bankProvince = $bankProvince;
    }

    /**
     * @return string
     */
    public function getBankCity(): string
    {
        return $this->bankCity;
    }

    /**
     * @param string $bankCity
     */
    public function setBankCity(string $bankCity): void
    {
        $this->bankCity = $bankCity;
    }

    /**
     * @return string
     */
    public function getChannel(): string
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     */
    public function setChannel(string $channel): void
    {
        $this->channel = $channel;
    }

}