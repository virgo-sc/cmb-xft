<?php

namespace CMB\XFT\Behalf;

/**
 * 代发付款方
 */
class Payer
{

    /**
     * 客户系统代发批次号
     * @var string
     */
    private $batchNo = '';

    /**
     * 代发协议
     * @var string
     */
    private $agencyAgreement = '';

    /**
     * 付款账号
     * @var string
     */
    private $account = '';

    /**
     * 付款户名
     * @var string
     */
    private $name = '';

    /**
     * 代发总金额
     * @var string
     */
    private $amount = '';

    /**
     * 代发总笔数
     * @var string
     */
    private $count = '';

    /**
     * 代发说明
     * @var string
     */
    private $mark = '';

    /**
     * 短信备注
     * @var string
     */
    private $phoneTxt = '';

    /**
     *预期时间
     * @var string
     */
    private $appointAt = '';

    /**
     * 预约日期
     * @var string
     */
    private $appointDate = '';

    /**
     * 支付人手机号
     * @var string
     */
    private $phone = '';

    /**
     * 支付有效期
     * @var string
     */
    private $payValidity = '';

    private $mark1 = '';

    private $mark2 = '';

    private $mark3 = '';



    public function generate(): array
    {
        $data =  [
            'BUSREF' => $this->batchNo,
            'PAYCNV' => $this->agencyAgreement,
            'PAYEAC' => $this->account,
            'PAYNAM' => $this->name,
            'CCYNBR' => 10,
            'TRXAMT' => $this->amount,
            'TRXNUM' => $this->count,
            'TRXRMK' => $this->mark,
            'NTFINF' => $this->phoneTxt,
            'ORDDAT' => $this->appointDate,
            'ORDTIM' => $this->appointAt,
            'CLTRMK' => $this->mark1,
            'CLTRM1' => $this->mark2,
            'CLTRM2' => $this->mark3,
        ];

        return $data;
    }

    /**
     * @param string $agencyAgreement
     */
    public function setAgencyAgreement(string $agencyAgreement): void
    {
        $this->agencyAgreement = $agencyAgreement;
    }

    /**
     * @param string $batchNo
     */
    public function setBatchNo(string $batchNo): void
    {
        $this->batchNo = $batchNo;
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount(string $account): void
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getCount(): string
    {
        return $this->count;
    }

    /**
     * @param string $count
     */
    public function setCount(string $count): void
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getMark(): string
    {
        return $this->mark;
    }

    /**
     * @param string $mark
     */
    public function setMark(string $mark): void
    {
        $this->mark = $mark;
    }

    /**
     * @return string
     */
    public function getPhoneTxt(): string
    {
        return $this->phoneTxt;
    }

    /**
     * @param string $phoneTxt
     */
    public function setPhoneTxt(string $phoneTxt): void
    {
        $this->phoneTxt = $phoneTxt;
    }

    /**
     * @return string
     */
    public function getAppointAt(): string
    {
        return $this->appointAt;
    }

    /**
     * @param string $appointAt
     */
    public function setAppointAt(string $appointAt): void
    {
        $this->appointAt = $appointAt;
    }

    /**
     * @return string
     */
    public function getAppointDate(): string
    {
        return $this->appointDate;
    }

    /**
     * @param string $appointDate
     */
    public function setAppointDate(string $appointDate): void
    {
        $this->appointDate = $appointDate;
    }

    /**
     * @return string
     */
    public function getMark1(): string
    {
        return $this->mark1;
    }

    /**
     * @param string $mark1
     */
    public function setMark1(string $mark1): void
    {
        $this->mark1 = $mark1;
    }

    /**
     * @return string
     */
    public function getMark2(): string
    {
        return $this->mark2;
    }

    /**
     * @param string $mark2
     */
    public function setMark2(string $mark2): void
    {
        $this->mark2 = $mark2;
    }

    /**
     * @return string
     */
    public function getMark3(): string
    {
        return $this->mark3;
    }

    /**
     * @param string $mark3
     */
    public function setMark3(string $mark3): void
    {
        $this->mark3 = $mark3;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPayValidity(): string
    {
        return $this->payValidity;
    }

    /**
     * @param string $payValidity
     */
    public function setPayValidity(string $payValidity): void
    {
        $this->payValidity = $payValidity;
    }
}