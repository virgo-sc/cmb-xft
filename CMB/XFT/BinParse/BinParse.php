<?php
namespace CMB\XFT\BinParse;

class BinParse {
    /**
     * @var string
     */
    private $account = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $amount = '';

    /**
     * @var string
     */
    private $no = '';


    public function generate(): array
    {

        return [
            'TRXSEQ' => $this->no,
            'EACNAM' => $this->name,
            'EACNBR' => $this->account,
            'TRXAMT' => $this->amount,
        ];
    }

    /**
     * @param string $account
     */
    public function setAccount(string $account): void
    {
        $this->account = $account;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $amount
     */
    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @param string $no
     */
    public function setNo(string $no): void
    {
        $this->no = $no;
    }

}