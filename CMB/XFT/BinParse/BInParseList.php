<?php

namespace CMB\XFT\BinParse;

class BInParseList
{
    private $list = [];

    public function append(BinParse $binParse)
    {
        $this->list[] = $binParse->generate();
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->list;
    }

}
