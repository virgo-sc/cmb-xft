<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Behalf\PayeeList;
use CMB\XFT\Behalf\Payer;
use CMB\XFT\Response\XFTBehalfPaymentResponse;

class XFTBehalfPaymentRequest extends RequestParameter
{
    protected $path = '/apm/EAIAGPAY';

    protected $method = 'post';

    protected $response = XFTBehalfPaymentResponse::class;

    protected $logTip = '代发支付';

    /**
     * @var $payer Payer
     */
    protected $payer;

    /**
     * @var $payeeList PayeeList
     */
    protected $payeeList;

    /**
     * @return Payer
     */
    public function getPayer(): Payer
    {
        return $this->payer;
    }

    /**
     * @param Payer $payer
     */
    public function setPayer(Payer $payer): void
    {
        $this->payer = $payer;
    }

    /**
     * @return PayeeList
     */
    public function getPayeeList(): PayeeList
    {
        return $this->payeeList;
    }

    /**
     * @param PayeeList $payeeList
     */
    public function setPayeeList(PayeeList $payeeList): void
    {
        $this->payeeList = $payeeList;
    }



    /**
     * 生成请求参数
     * @return array
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIAGPAYX' => [
                $this->payer->generate()
            ],
            'EAIPAYDTX' => $this->payeeList->generate(),
            'EAIAGTELX' => [
                [
                    'PAYTEL' => $this->payer->getPhone(),
                    'PAYVLD' => $this->payer->getPayValidity()
                ]
            ]
        ];
    }
}