<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfInfoPDFApplyResponse;

class XFTBehalfInfoPDFApplyRequest extends RequestParameter
{
    protected $path = '/apm/EAIREPDF';

    protected $method = 'post';

    protected $response = XFTBehalfInfoPDFApplyResponse::class;

    protected $logTip = '生成明细对账单PDF';

    /**
     * 代发协议号
     * @var string $agreement
     */
    private $agreement = '';

    /**
     * 客户系统代发批次号
     * @var string $batchNo
     */
    private $batchNo = '';

    /**
     * 薪福通代发批次号
     * @var string $batchTransId
     */
    private $batchTransId = '';

    /**
     * 客户批次的代发明细序号
     * @var string $orderId
     */
    private $orderId = '';
    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIREPDFX ' => [[
                'CHOTYP' => 'D',
                'CNVNBR' => $this->agreement,
                'PRTNUM' => '1',
                'FILAGT' => 'N',
                'FILCIF' => 'N',
                'FILNAM' => 'N',
                'FILNBR' => 'Y',
                'FILTRS' => 'N',
            ]],
            'EAISMYDTX' => [[
                'TRSREF' => $this->batchTransId,
                'BUSREF' => $this->batchNo,
                'TRXSEQ' => $this->orderId
            ]],
        ];
    }

    /**
     * @param string $agreement
     */
    public function setAgreement(string $agreement): void
    {
        $this->agreement = $agreement;
    }

    /**
     * @param string $batchNo
     */
    public function setBatchNo(string $batchNo): void
    {
        $this->batchNo = $batchNo;
    }

    /**
     * @param string $batchTransId
     */
    public function setBatchTransId(string $batchTransId): void
    {
        $this->batchTransId = $batchTransId;
    }

    /**
     * @param string $orderId
     */
    public function setOrderId(string $orderId): void
    {
        $this->orderId = $orderId;
    }
}