<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTPayUserQueryResponse;

class XFTPayUserQueryRequest extends RequestParameter
{
    protected $path = '/apm/EAIPAYUS';

    protected $method = 'post';

    protected $format = 'body';

    protected $response = XFTPayUserQueryResponse::class;

    protected $logTip = '查询可用支付用户';

    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): string
    {
        return "{}";
    }
}