<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfMoneyQueryResponse;

class XFTBehalfMoneyQueryRequest extends RequestParameter
{
    protected $path = '/apm/EAISPQRY';

    protected $method = 'post';

    protected $response = XFTBehalfMoneyQueryResponse::class;

    protected $logTip = '代发资金查询';

    protected $agencyAgreement;

    /**
     * 生成请求参数
     * @return array
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EASPCQRYX' => [[
                'PAYCNV' => $this->agencyAgreement
            ]],
        ];
    }

    /**
     * @param mixed $agencyAgreement
     */
    public function setAgencyAgreement($agencyAgreement): void
    {
        $this->agencyAgreement = $agencyAgreement;
    }
}