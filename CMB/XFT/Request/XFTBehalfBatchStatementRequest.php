<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfBatchStatementResponse;

class XFTBehalfBatchStatementRequest extends RequestParameter
{
    protected $path = '/apm/EAIRECST';

    protected $method = 'post';

    protected $response = XFTBehalfBatchStatementResponse::class;

    protected $logTip = '代发明细查询';

    private $page = '1';

    private $pageSize = '10000';

    /**
     * 申请开始日期
     * @var string $beginDate
     */
    private $beginDate = '';

    /**
     * 申请结束日期
     * @var string $endAtDate
     */
    private $endAtDate = '';

    /**
     * 代发编号
     * @var string
     */
    private $agreement = '';

    /**
     * 客户系统代发批次号
     * @var string $batchNo
     */
    private $batchNo = '';

    /**
     * 薪福通代发批次号
     * @var string $batchTransId
     */
    private $batchTransId = '';

    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIRPLSTX' => [[
                'STADAT' => $this->beginDate,
                'ENDDAT' => $this->endAtDate,
                'PAYCNV' => $this->agreement,
                'TRSREF' => $this->batchTransId,
                'BUSREF' => $this->batchNo,
            ]],
            'SYPAGINFY' => [[
                'PAGNBR' => $this->page,
                'PGENUM' => $this->pageSize
            ]]
        ];
    }

    /**
     * @param string $page
     */
    public function setPage(string $page): void
    {
        $this->page = $page;
    }

    /**
     * @param string $pageSize
     */
    public function setPageSize(string $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @param string $beginDate
     */
    public function setBeginDate(string $beginDate): void
    {
        $this->beginDate = $beginDate;
    }

    /**
     * @param string $endAtDate
     */
    public function setEndAtDate(string $endAtDate): void
    {
        $this->endAtDate = $endAtDate;
    }

    /**
     * @param string $agreement
     */
    public function setAgreement(string $agreement): void
    {
        $this->agreement = $agreement;
    }

    /**
     * @param string $batchNo
     */
    public function setBatchNo(string $batchNo): void
    {
        $this->batchNo = $batchNo;
    }

    /**
     * @param string $batchTransId
     */
    public function setBatchTransId(string $batchTransId): void
    {
        $this->batchTransId = $batchTransId;
    }
}