<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTAgencyAgreementQueryResponse;

class XFTAgencyAgreementQueryRequest extends RequestParameter
{
    protected $path = '/apm/EAIAGCNV';

    protected $method = 'post';

    protected $response = XFTAgencyAgreementQueryResponse::class;

    protected $logTip = '代发协议查询服务';

    /**
     * 生成请求参数
     * @return array
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIAGCNVX' => [[
                'BUSTYP' => '0'
            ]],
        ];
    }
}