<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\BinParse\BInParseList;
use CMB\XFT\Response\XFTBinParseResponse;

class XFTBinParseRequest extends RequestParameter
{
    protected $path = '/apm/EAIDTCHK';

    protected $method = 'post';

    protected $response = XFTBinParseResponse::class;

    protected $logTip = '4.11.3.卡BIN解析';

    private $binParseList = [];

    /**
     * 生成请求参数
     * @return array
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIDTCHKX' => $this->binParseList
        ];
    }

    public function setList(BInParseList $bInParseList)
    {
        $this->binParseList = $bInParseList->getList();
    }

}