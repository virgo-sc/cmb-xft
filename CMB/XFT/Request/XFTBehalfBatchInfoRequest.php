<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfBatchInfoResponse;

class XFTBehalfBatchInfoRequest extends RequestParameter
{
    protected $path = '/apm/EAIDTLST';

    protected $method = 'post';

    protected $response = XFTBehalfBatchInfoResponse::class;

    protected $logTip = '代发明细查询';


    private $page = '1';

    private $pageSize = '10000';

    /**
     * 客户系统代发批次号
     * @var string $batchNo
     */
    private $batchNo = '';

    /**
     * 薪福通代发批次号
     * @var string $batchTransId
     */
    private $batchTransId = '';

    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIDTLSTX' => [[
                'BUSREF' => $this->batchNo,
                'TRSREF' => $this->batchTransId,
            ]],
            'SYPAGINFY' => [[
                'PAGNBR' => $this->page,
                'PGENUM' => $this->pageSize
            ]]
        ];
    }

    /**
     * @param string $batchNo
     */
    public function setBatchNo(string $batchNo): void
    {
        $this->batchNo = $batchNo;
    }

    /**
     * @param string $batchTransId
     */
    public function setBatchTransId(string $batchTransId): void
    {
        $this->batchTransId = $batchTransId;
    }

    /**
     * @param string $page
     */
    public function setPage(string $page): void
    {
        $this->page = $page;
    }

    /**
     * @param string $pageSize
     */
    public function setPageSize(string $pageSize): void
    {
        $this->pageSize = $pageSize;
    }
}