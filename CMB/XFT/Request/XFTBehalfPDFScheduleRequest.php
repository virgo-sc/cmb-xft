<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfPDFScheduleResponse;

class XFTBehalfPDFScheduleRequest extends RequestParameter
{
    protected $path = '/apm/EAIQRPST';

    protected $method = 'post';

    protected $response = XFTBehalfPDFScheduleResponse::class;

    protected $logTip = '查询PDF的生成的状态/进度';

    /**
     * Redis键值
     * @var string $redisKey
     */
    private $redisKey = '';

    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIQRPSTX' => [[
                'RESKEY' => $this->redisKey,
            ]],
        ];
    }

    /**
     * @param string $redisKey
     */
    public function setRedisKey(string $redisKey): void
    {
        $this->redisKey = $redisKey;
    }
}