<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfBatchQueryListResponse;

class XFTBehalfBatchQueryListRequest extends RequestParameter
{
    protected $path = '/apm/EAIMALST';

    protected $method = 'post';

    protected $response = XFTBehalfBatchQueryListResponse::class;

    protected $logTip = '代发概要查询';

    private $page = '1';

    private $pageSize = '10000';

    /**
     * 申请开始日期
     * @var string $beginDate
     */
    private $beginDate = '';

    /**
     * 申请结束日期
     * @var string $endAtDate
     */
    private $endAtDate = '';

    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIMALSTX' => [[
                'STADAT' => $this->beginDate,
                'ENDDAT' => $this->endAtDate,
            ]],
            'SYPAGINFY' => [[
                'PAGNBR' => $this->page,
                'PGENUM' => $this->pageSize
            ]]
        ];
    }

    /**
     * @param string $beginDate
     */
    public function setBeginDate(string $beginDate): void
    {
        $this->beginDate = $beginDate;
    }

    /**
     * @param string $endAtDate
     */
    public function setEndAtDate(string $endAtDate): void
    {
        $this->endAtDate = $endAtDate;
    }

    /**
     * @param string $page
     */
    public function setPage(string $page): void
    {
        $this->page = $page;
    }

    /**
     * @param string $pageSize
     */
    public function setPageSize(string $pageSize): void
    {
        $this->pageSize = $pageSize;
    }
}