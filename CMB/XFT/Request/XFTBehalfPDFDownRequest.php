<?php

namespace CMB\XFT\Request;

use CMB\Http\RequestParameter;
use CMB\XFT\Response\XFTBehalfPDFDownResponse;

class XFTBehalfPDFDownRequest extends RequestParameter
{
    protected $path = '/apm/EAIPDFDL';

    protected $method = 'post';

    protected $response = XFTBehalfPDFDownResponse::class;

    protected $logTip = '明细对账单PDF';

    /**
     * Redis键值
     * @var string $redisKey
     */
    private $redisKey = '';

    /**
     * 文件分块编号
     * @var int $blockNo
     */
    private $blockNo = 0;
    /**
     * 生成请求参数
     * @author xis
     */
    public function generate(): array
    {
        return [
            'EAIPDFDLX' => [[
                'RESKEY' => $this->redisKey,
            ]],
            'EAIPDFDLY' => [[
                'FILINX' => $this->blockNo
            ]]
        ];
    }

    /**
     * @param string $redisKey
     */
    public function setRedisKey(string $redisKey): void
    {
        $this->redisKey = $redisKey;
    }

    /**
     * @param int $blockNo
     */
    public function setBlockNo(int $blockNo): void
    {
        $this->blockNo = $blockNo;
    }
}