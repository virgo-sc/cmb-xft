<?php

namespace Alipay\AlipayFund\Info;

/**
 * 收款方信息
 */
class PayeeList
{
    /**
     * 收款信息列表
     * @var array
     */
    private $transOrderList = [];

    private $totalCount = '0';

    private $totalAmount = '0';

    /**
     * @param Parties $parties
     * @param string $outBizNo
     * @param string $amount
     * @param string $remark
     * @author xis
     */
    public function appendPayee(Parties $parties, string $outBizNo, string $amount, string $remark = '')
    {
        $this->totalCount = bcadd($this->totalCount, '1');
        $this->totalAmount = bcadd($this->totalAmount, $amount, 2);

        $payee = [
            'out_biz_no' => $outBizNo,
            'trans_amount' => $amount,
            'remark' => $remark,
            'payee_info' => $parties
        ];
        $this->transOrderList[] = $payee;
    }

    /**
     * @return string
     */
    public function getTotalAmount(): string
    {
        return $this->totalAmount;
    }

    /**
     * @return string
     */
    public function getTotalCount(): string
    {
        return $this->totalCount;
    }

    public function generate(): array
    {
        $list = [];
        foreach ($this->transOrderList as $key => $item) {
            $item['payee_info'] = $item['payee_info']->generate();
            $list[] = $item;
        }
        return $list;
    }
}
