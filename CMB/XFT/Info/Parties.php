<?php

namespace Alipay\AlipayFund\Info;

use Exception;

/**
 * 付款方信息
 */
class Parties
{
    /**
     * 参与方的唯一标识
     * @var string
     */
    private $identity;

    /**
     *参与方的标识类型
     * @var string
     */
    private $identityType;

    /**
     * 参与方真实姓名
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $bankcardExtInfo;

    /**
     * @var string
     */
    private $merchantUserInfo;

    /**
     * @var string
     */
    private $extInfo;

    /**
     * @param string $identity
     * @param string $name
     * @param string $identityType
     * @throws Exception
     */
    public function __construct(string $identity, string $name = '', string $identityType = 'ALIPAY_USER_ID')
    {
        $this->identity = $identity;
        if ($identityType == 'ALIPAY_LOGON_ID') {
            $this->typeLogonId();
        } else {
            $this->typeUserId();
        }
        $this->name = $name;

        if ($this->identityType == 'ALIPAY_LOGON_ID') {
            if (!$this->name) {
                throw new Exception('当identity_type=ALIPAY_LOGON_ID时，name必填。 ');
            }
        }
    }

    public function typeUserId()
    {
        $this->identityType = 'ALIPAY_USER_ID';
    }

    public function typeLogonId()
    {
        $this->identityType = 'ALIPAY_LOGON_ID';
    }

    /**
     * @param array $merchantUserInfo
     * @return $this
     * @author xis
     */
    public function setMerchantUserInfo(array $merchantUserInfo): self
    {
        $this->merchantUserInfo = json_encode($merchantUserInfo, JSON_UNESCAPED_UNICODE);
        return $this;
    }

    /**
     * @param array $extInfo
     * @return $this
     * @author xis
     */
    public function setExtInfo(array $extInfo): self
    {
        $this->extInfo = json_encode($extInfo, JSON_UNESCAPED_UNICODE);
        return $this;
    }

    /**
     * @param array $bankcardExtInfo
     * @return $this
     * @author xis
     */
    public function setBankcardExtInfo(array $bankcardExtInfo): self
    {
        $this->bankcardExtInfo = json_encode($bankcardExtInfo, JSON_UNESCAPED_UNICODE);
        return $this;
    }

    public function generate() :array
    {
        return [
            'identity' => $this->identity,
            'identity_type' => $this->identityType,
            'name' => $this->name
        ];
    }
}
