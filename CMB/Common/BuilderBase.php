<?php

namespace CMB\Common;

use CMB\Http\Request;
use CMB\Http\RequestParameter;
use CMB\Http\Response;
use Exception;
use http\Exception\BadMethodCallException;

/**
 * 执行请求基础类
 */
class BuilderBase extends Construct
{
    /**
     * 请求类
     * @var Request
     */
    private $request;

    /**
     * 执行请求--构建请求参数
     * @param RequestParameter $requestParameter
     * @return Response
     * @throws Exception
     * @author xis
     */
    public function execute(RequestParameter $requestParameter): Response
    {
        //仅仅是dome
        //需要继承后重写
        try {
            $requestParameter->setParameter($this->generate($requestParameter));

            $this->sign($requestParameter);

            return $this->request($requestParameter);
        } catch (Exception $exception) {
            return $this->exceptionResponse($requestParameter->getResponse(), $exception->getMessage());
        }
    }

    /**
     * 请求参数生成
     * @param RequestParameter $requestParameter
     * @return array
     */
    public function generate(RequestParameter $requestParameter): array
    {
        throw new BadMethodCallException(self::class . ':generate not be inheritance');
    }

    /**
     * 生成签名
     * @param RequestParameter $requestParameter
     * @return mixed
     */
    public function sign(RequestParameter $requestParameter)
    {
        throw new BadMethodCallException(self::class . ':sign not be inheritance');
    }

    public function exceptionResponse($response, $errorMessage): Response
    {
        if (!class_exists($response)) {
            $response = new Response();
        } else {
            $response = new $response;
        }

        $response->setError($errorMessage);

        return $response;
    }

    /**
     * 执行请求--构建执行参数
     * @param RequestParameter $requestParameter
     * @return Response
     */
    protected function request(RequestParameter $requestParameter): Response
    {
        //初始化
        if (!$this->request instanceof Request) {
            $this->request = new Request;
            $this->request->setTimeout($this->timeout);
            $this->request->setDebug($this->httpDebug);
            $this->request->setLogsDir($this->logsDir);

            $this->request->setHeaders($requestParameter->getHeaders());
        }

        if ($requestParameter->getHeaders()) {
            $this->request->setHeaders($requestParameter->getHeaders());
        }
        $this->request->setLogTip($requestParameter->getLogTip());

        $param = $requestParameter->generate();

        $client = $this->request;

        $client->setUrl($this->url);

        $client->setMethod($requestParameter->getMethod());

        //get
        if (strtoupper($requestParameter->getMethod()) == 'GET') {
            $client->setGetParam($param);
        }
        //post
        if (strtoupper($requestParameter->getMethod()) == 'POST') {

            if ($requestParameter->getFormat()) {
                $this->format = $requestParameter->getFormat();
            }

            if ($this->format == 'json') {
                $client->setJson($param);
            }
            if ($this->format == 'formData') {
                $client->setFormData($param);
            }
            if ($this->format == 'body') {
                $client->setBody($param);
            }
        }

        return $client->request($requestParameter->getResponse());
    }
}
