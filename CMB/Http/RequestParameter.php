<?php

namespace CMB\Http;

class RequestParameter extends Request
{

    /**
     * response
     * @var string $response
     */
    protected $response = '';

    /**
     * 请求路由
     * @var string $path
     */
    protected $path = '';

    /**
     * 请求参数
     * @var array|string
     */
    protected $parameter;

    public function getParameter()
    {
        return $this->parameter;
    }

    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return array|string
     */
    public function generate()
    {
        return [];
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
